import Net from 'net';
import { EventEmitter } from 'events'

export default class Socket extends EventEmitter {
    constructor(){
        super()
        this.socket = new Net.socket();
        this.socket.setEncoding('ascii');
        this.socket.on('data', (msg) => this.process(msg));
        this.socket.on('connect', () => {
            this.connected = true;
        });
        this.socket.on('end',() => {
            this.connected = false;
        });
        
        // Connection Info/Status
        this.connected = false;
        this.waitingConnection = false;
        this.inError = false;
        this.port = 9993;
        this.address = "192.168.100.4";
        
    }

    // Process will take in the packet information and process it and emit the appropriate event.
    // Private function
    async process(msg) {
        if(this.inError) return;
        
    }

    sendCommand(command,data){
        if(!this.connected) return;
        
    }

    async connect(){
        
    }

    async disconnect(){
        this.connected = false;
    }

    async destroy(){
        await this.disconnect()
        this.socket.close();
    }
}
