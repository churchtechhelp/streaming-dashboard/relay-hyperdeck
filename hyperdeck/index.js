import EventEmitter from 'events';
import Path from 'path';
import Fs  from 'fs';
import Socket from './socket.js';
import onChange from 'on-change';
import _ from 'lodash';
//import { loadavg } from 'os';


// Command Loader
// Basically need to go import everything in the commands folder and put into constant with reference to command if get, and name if put.
// Goal is to be like Vue.js Functional API to make command mantinence easy

// Custom Exceptions
class PreparerException extends Error {
    constructor(message){
        super(message);
        this.name = "PreparerException"
    }
}

class ProcessorException extends Error {
    constructor(message){
        super(message);
        this.name = "ProcessorException"
    }
}


// Main Class
export default class Atem extends EventEmitter {
    constructor(){
        super();

        this.commands = {};
        this.commandPackets = {};
        this.loaded = false;
        this.connected = false;
        this.state = {
            me: [{},{}],
        };
        this.socket = new Socket();
        this.socket.on('message', (command,data) => this.recvCommand(command,data));
        this.socket.on('connect', () => this.onConnect());
        this.socket.on('disconnect', () => this.onDisconnect());
        this.PreparerException = PreparerException;
        this.ProcessorException = ProcessorException;
        
        // File Transfer State
        this.currentTransfer = null;
        this.nextTransferId = 0;
        this.fileTransfers = {};
    }

    sendCommand(command,data){
        // sends command on socket
        console.log("number of preparers", Object.keys(this.packetPreparers).length);
        if(command in this.packetPreparers){
            console.log("Sending socket command",command);
            this.socket.sendCommand(this.commands[command].command,this.packetPreparers[command].call(this,data));
        } else {
            console.log("No handler for packet send command", command);
        }
    }

    recvCommand(command,data){
        //console.log("Received command",command);
        if(this.packetProcessors[command]) {
            this.packetProcessors[command].call(this,data);
        } else {
            console.log("no handler for packet command",command);
        }
    }

    getState(){
        return this.state;
    }

    isConnected(){
        return this.connected;
    }

    async load() {
        // Inspired by Vuex
        let commands = {};
        let me = {};
        const currentDir = Path.resolve('');
        console.log("Current Directory is",currentDir);
        const path = Path.join(currentDir, "atem/commands");
        const files = Fs.readdirSync(path);
        
        // Clear arrays
        this.packetProcessors = {};
        this.packetPreparers = {};
        this.commands = {};
        this.state = {};

        for (const file of files) {
            //console.log("Loading file",file);
            let command = await import("./commands/" + file);
            command = command.default;
            if(!command.type) {
                throw "Failed to import";
            }
            // Validate basic requirements
            if(!(command.name && typeof command.name == 'string')){
                console.log(file,"does not have a name");
                throw "Missing Name in file " + file;
            } 

            if(command.name in this.commands){
                throw "Duplicate Command Name: " + command.name
            }

            if(!(command.type && typeof command.type == 'string')){
                console.log(file,"does not have a type")
            }

            // Outgoing
            this.commands[command.name] = command;
            // Incoming packet decoding
            if(command.type == "GET" && command.process && typeof command.process == 'function'){
                if(command.command in this.packetProcessors){
                    throw "Duplicate Getter Command " + command.command;
                }
                this.packetProcessors[command.command] = command.process;
            }
            if(command.type == "SET" && command.prepare && typeof command.prepare == 'function'){
                if(command.command in this.packetPreparers){
                    throw "Duplicate Set Command " + command.command;
                }
                this.packetPreparers[command.name] = command.prepare;
            }
            // State Setup
            if(command.type == "GET" && command.state){
                // Parse State by adding all objects to the state.
                // TODO: Conflict Detection in state
                if(command.me){
                    // Currently a maximum of two ME's
                    me = _.merge(me, command.state());
                } else {
                    this.state = _.merge(this.state, command.state());
                }
            }
        }

        this.state.me = [me,me];

        this.defaultState = this.state;

        console.log("Transitions",this.state.me[0].transitions);

        this.loaded = true;
    }

    async connect(){
        if(!this.loaded) return false;
        if(this.connected) {
            console.log("Already connected");
        }
        await this.socket.connect();
    }

    async sendFile(file, target){
        
    }

    onConnect(){
        // The switcher will automatically send all the right information.
        // We will just need to let the websocket clients know that we are connected and the state is good
        console.log("Connected to ATEM");
        this.emit("connected");
        this.connected = true;
        this.state.connected = true;

        // We also need to watch the state to efficiently send the changes
        this.state = onChange(this.state,(path,value,lastValue)=>this.onStateChange(path,value,lastValue))
    }

    onDisconnect(){
        // Let the websocket client know about the difference
        console.log("Disconnect from ATEM")
        this.emit("disconnected")
        this.connected = false;
        this.state.connected = false;

        // Since there are no more changes, then we will stop listening to state changes.
        onChange.unsubscribe(this.state);
    }

    onStateChange(path,value,lastValue){
        let changePacket = {};
        changePacket[path] = value;
        this.emit('stateChanged',changePacket);
    }
}
