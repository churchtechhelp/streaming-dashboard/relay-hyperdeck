import Atem from './atem/index.js';
import WebSocket from 'ws';

const atem = new Atem();
const ws = new WebSocket.Server({ port: 4455 });

var reconnectInterval = null;

// Websocket Opened: Send initial data
ws.on('connection', function open(client,request) {
    console.log("WS Client Connected");
    
    // Convert state to string and send
    if(atem.isConnected()) {
        let packet = {
            event: "connected",
            value: atem.getState(),
        };
        console.log("Sending current state");
        client.send(JSON.stringify(packet));
    } else {
        console.log("Atem not connected");
    }

    // Websocket Command
    client.on('message', function incoming(message) {
        console.log("WS Message received",message)
        // convert from serialized string to object
        const command = JSON.parse(message);
        atem.sendCommand(command.command,command.data);
    });
});

// Atem Connected
atem.on('connected', () => {
    if(reconnectInterval){
        clearInterval(reconnectInterval);
    }

    let changePacket = {
        event: "connected",
        value: atem.getState(),
    };

    ws.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    });
});

atem.on('disconnected', () => {
    let changePacket = {
        event: "disconnected",
    };

    ws.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    });

    reconnectInterval = setInterval(function(){
        console.log("Attempting reconnect");
        atem.connect();
    },5000);
})
 
// Atem State change: Send websocket update
// TODO: Only send difference. Technically only have to look for one?
atem.on('stateChanged', function(change) {
    //console.log("State Change:",change);
    let changePacket = {
        event: "stateChanged",
        value: change,
    }

    // Convert to string and send to all clients
    ws.clients.forEach((client)=> {
        client.send(JSON.stringify(changePacket));
    })
});

( async () => {
    await atem.load();
    await atem.connect();
})();