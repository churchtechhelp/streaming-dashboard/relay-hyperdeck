# Streamer Dashboard ATEM Connector

This Node.js app bridges the ATEM (UDP) and the Streamer Dashboard (Websocket).

This project implements the ATEM protocol in a maintainable way for the dashboard.
Other libraries were either incomplete, their current release did not work, or they were in another language.

This project is licensed under the GNU GPL v3.0

## Installing and Running

1. Clone this repository using git Clone or download a zip file.
2. In the project's folder, run `npm install` to grab the dependencies
3. Run the program: `node ./index.js`

## Attribution

This project has referenced several other projects to understand the ATEM protocol.

https://www.skaarhoj.com/fileadmin/BMDPROTOCOL.html
https://github.com/nrkno/tv-automation-atem-connection
https://github.com/petersimonsson/libqatemcontrol